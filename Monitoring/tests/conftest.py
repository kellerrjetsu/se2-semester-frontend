import pytest
from Monitoring import Monitoring as flask_app

@pytest.fixture
def app():
    yield flask_app


@pytest.fixture
def client():
    app = flask_app.create_app()
    app.config["TESTING"] = True
    with app.test_client() as client:
        yield client


