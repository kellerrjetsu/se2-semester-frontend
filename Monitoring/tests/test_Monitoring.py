import json
import requests
from Monitoring.Monitoring import create_figure
from matplotlib.figure import Figure

#test that the main page in the website is running
def test_main(client):
    response = client.get('/')
    assert response.status_code == 200

#test that the admin page in the website is running
def test_admin(client):
    response = client.get('/admin/')
    assert response.status_code == 200

#test that the about page in the website is running
def test_about(client):
    response = client.get('/about/')
    assert response.status_code == 200

#test that the API can take requests
def test_API():
    response = requests.get("https://ghibliapi.herokuapp.com/films")
    assert response.status_code == 200

fig = create_figure()
allaxes = fig.get_axes()

#test if figure has all plots (12)
def test_plot_count():
    numPlots = len(allaxes)
    assert numPlots == 12

#test if the first graph has been created correctly
def test_movie_plot_created():
    xlabel = allaxes[0].get_xlabel()
    ylabel = allaxes[0].get_ylabel()
    assert (xlabel == "Running Time" and ylabel == "Rating")