######################################################################################################################################################
#   Project:                        Frontend of the Monitoring
#   Class:                          CSCI 4350
#   Last Modified Date:             4/3/2021
#   Last Modified By:               David Hopland
#   Purpose:                        Frontend of the monitoring teams project. This is where users can see the data about the sister teams database is a visually pleasing way.
######################################################################################################################################################
from flask import Flask, send_file, Response, render_template, request, redirect
import matplotlib.pyplot as plt
from io import BytesIO
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import requests
import json
import webbrowser

#this is where we got our dummy data to show that our graphics will display data. the request gets a json object for the API of our random website
response = requests.get("https://ghibliapi.herokuapp.com/films").json()

size = len(response)                                #get the score and running times (the dummy data)

rtScore_runningTime = []                            #array that will hold the running time
for i in range(size):
    rtScore_runningTime.append([int(response[i]['rt_score']), int(response[i]['running_time'])])        #appends the next response to the array

rtScore_runningTime.sort(key=lambda x:x[1])         #sort the array
rt_score = []                                       #holds the score y-axis value
runningTimes = []                                   #holds the running time x-axis value
for i in range(len(rtScore_runningTime)):
    rt_score.append(rtScore_runningTime[i][0])      #add the scores to the rt_socre array
    runningTimes.append(rtScore_runningTime[i][1])  #add the running times to the runningTimes array 

#create the application using flask
def create_app():
    app = Flask(__name__)

    #this is the home page of the website named index.html
    @app.route("/")
    def home():
        return render_template("index.html")

    #this is the route that will redirect to the about page
    @app.route("/about/")
    def about():
        return render_template("about.html")

    #this is the route that will redirect to the admin login page
    @app.route("/admin/")
    def admin():
        return render_template("admin.html")

    #this will display the plots for the metrics created in the next section
    @app.route("/plot.png")
    def plot_png():
        fig = create_figure()
        fig.patch.set_facecolor('#3F474C')
        output = BytesIO()
        FigureCanvas(fig).print_png(output)
        return Response(output.getvalue(), mimetype='image/png')

    return app

app = create_app()
    
#this is the definition of the figure that will have all the metrics. They are all in one figure as subplots to keep them nicely formatted
#these are very similar so the comments on the first subplot will give context for  all the following subplots
def create_figure():
    fig = Figure(figsize=(15, 10))        #sets the size of the whole figure 
    axis = fig.add_subplot(4, 4, 1)     #adds a subplot to the figure at the given location
    axis.set_ylim([0, 100])                 #sets the limit of the y-axis
    x = runningTimes        #setting the value of the x-axis
    y = rt_score                   #sets the value of the y-axis
    #x = [1, 2, 3, 4]
    #y = [2, 5, 6, 2]
    axis.title.set_text("Ratings per Running Time")    #title of the subplot
    axis.set_xlabel("Running Time")                             #name of the x-axis
    axis.set_ylabel("Rating")                                          #name of the y-axis
    axis.set_facecolor('#1F2326')                                    #set the color to the given hex value
    axis.xaxis.label.set_color('white')                            #x label color 
    axis.yaxis.label.set_color('white')                            #y label color
    axis.title.set_color('white')                                       #title color
    axis.spines['bottom'].set_color('white')                  #spine bottom color to white
    axis.spines['left'].set_color('white')                         #left to white
    axis.tick_params(axis='x', colors='white')              #set x parameters to white
    axis.tick_params(axis='y', colors='white')              #set y parameters to white
    axis.grid(color='white', linestyle='--', alpha=0.5)  #sets the grid to white
    axis.plot(x, y)                                                            #plots the subplot

    axis = fig.add_subplot(4, 4, 2)
    x = [1, 2, 3, 4]            #currently dummy data
    y = [1, 2, 3, 4]
    axis.title.set_text("Free Memory")
    axis.set_xlabel("Time")
    axis.set_ylabel("Memory (MB)")
    axis.set_facecolor('#1F2326')
    axis.xaxis.label.set_color('white')
    axis.yaxis.label.set_color('white')
    axis.title.set_color('white')
    axis.spines['bottom'].set_color('white')
    axis.spines['left'].set_color('white')
    axis.tick_params(axis='x', colors='white')
    axis.tick_params(axis='y', colors='white')
    axis.grid(color='white', linestyle='--', alpha=0.5)
    axis.plot(x, y)

    axis = fig.add_subplot(4, 4, 3)
    x = [1, 2, 3, 4]            #currently dummy data
    y = [6, 2, 4, 1]
    axis.title.set_text("Free Storage Space")
    axis.set_xlabel("Time")
    axis.set_ylabel("Storage Space (MB)")
    axis.set_facecolor('#1F2326')
    axis.xaxis.label.set_color('white')
    axis.yaxis.label.set_color('white')
    axis.title.set_color('white')
    axis.spines['bottom'].set_color('white')
    axis.spines['left'].set_color('white')
    axis.tick_params(axis='x', colors='white')
    axis.tick_params(axis='y', colors='white')
    axis.grid(color='white', linestyle='--', alpha=0.5)
    axis.plot(x, y)

    axis = fig.add_subplot(4, 4, 4)
    x = [1, 2, 3, 4]            #currently dummy data
    y = [5, 5, 3, 4]
    axis.title.set_text("Network Receive Throughput")
    axis.set_xlabel("Time")
    axis.set_ylabel("Recieve Throughput (MB/second)")
    axis.set_facecolor('#1F2326')
    axis.xaxis.label.set_color('white')
    axis.yaxis.label.set_color('white')
    axis.title.set_color('white')
    axis.spines['bottom'].set_color('white')
    axis.spines['left'].set_color('white')
    axis.tick_params(axis='x', colors='white')
    axis.tick_params(axis='y', colors='white')
    axis.grid(color='white', linestyle='--', alpha=0.5)
    axis.plot(x, y)


    axis = fig.add_subplot(4, 4, 5)
    x = [1, 2, 3, 4]            #currently dummy data
    y = [2, 2, 5, 4]
    axis.title.set_text("Network Transmit Throughput")
    axis.set_xlabel("Time")
    axis.set_ylabel("Transmit Throughput (MB/second)")
    axis.set_facecolor('#1F2326')
    axis.xaxis.label.set_color('white')
    axis.yaxis.label.set_color('white')
    axis.title.set_color('white')
    axis.spines['bottom'].set_color('white')
    axis.spines['left'].set_color('white')
    axis.tick_params(axis='x', colors='white')
    axis.tick_params(axis='y', colors='white')
    axis.grid(color='white', linestyle='--', alpha=0.5)
    axis.plot(x, y)

    axis = fig.add_subplot(4, 4, 6)
    x = [1, 2, 3, 4]            #currently dummy data
    y = [1, 5, 5, 1]
    axis.title.set_text("Database Connections")
    axis.set_xlabel("Time")
    axis.set_ylabel("Number of Connections")
    axis.set_facecolor('#1F2326')
    axis.xaxis.label.set_color('white')
    axis.yaxis.label.set_color('white')
    axis.title.set_color('white')
    axis.spines['bottom'].set_color('white')
    axis.spines['left'].set_color('white')
    axis.tick_params(axis='x', colors='white')
    axis.tick_params(axis='y', colors='white')
    axis.grid(color='white', linestyle='--', alpha=0.5)
    axis.plot(x, y)

    axis = fig.add_subplot(4, 4, 7)
    x = [1, 2, 3, 4]            #currently dummy data
    y = [2, 1, 2, 1]
    axis.title.set_text("Write IOPS")
    axis.set_xlabel("Time")
    axis.set_ylabel("IOPS (count/second)")
    axis.set_facecolor('#1F2326')
    axis.xaxis.label.set_color('white')
    axis.yaxis.label.set_color('white')
    axis.title.set_color('white')
    axis.spines['bottom'].set_color('white')
    axis.spines['left'].set_color('white')
    axis.tick_params(axis='x', colors='white')
    axis.tick_params(axis='y', colors='white')
    axis.grid(color='white', linestyle='--', alpha=0.5)
    axis.plot(x, y)

    axis = fig.add_subplot(4, 4, 8)
    x = [1, 2, 3, 4]            #currently dummy data
    y = [2, 3, 2, 3]
    axis.title.set_text("Read IOPS")
    axis.set_xlabel("Time")
    axis.set_ylabel("IOPS (count/second)")
    axis.set_facecolor('#1F2326')
    axis.xaxis.label.set_color('white')
    axis.yaxis.label.set_color('white')
    axis.title.set_color('white')
    axis.spines['bottom'].set_color('white')
    axis.spines['left'].set_color('white')
    axis.tick_params(axis='x', colors='white')
    axis.tick_params(axis='y', colors='white')
    axis.grid(color='white', linestyle='--', alpha=0.5)
    axis.plot(x, y)

    axis = fig.add_subplot(4, 4, 9)
    x = [1, 2, 3, 4]            #currently dummy data
    y = [2, 2, 2, 1]
    axis.title.set_text("Write Throughput")
    axis.set_xlabel("Time")
    axis.set_ylabel("Throughput (MB/second)")
    axis.set_facecolor('#1F2326')
    axis.xaxis.label.set_color('white')
    axis.yaxis.label.set_color('white')
    axis.title.set_color('white')
    axis.spines['bottom'].set_color('white')
    axis.spines['left'].set_color('white')
    axis.tick_params(axis='x', colors='white')
    axis.tick_params(axis='y', colors='white')
    axis.grid(color='white', linestyle='--', alpha=0.5)
    axis.plot(x, y)

    axis = fig.add_subplot(4, 4, 10)
    x = [1, 2, 3, 4]            #currently dummy data
    y = [4, 6, 4, 1]
    axis.title.set_text("Read Throughput")
    axis.set_xlabel("Time")
    axis.set_ylabel("Throughput (MB/second)")
    axis.set_facecolor('#1F2326')
    axis.xaxis.label.set_color('white')
    axis.yaxis.label.set_color('white')
    axis.title.set_color('white')
    axis.spines['bottom'].set_color('white')
    axis.spines['left'].set_color('white')
    axis.tick_params(axis='x', colors='white')
    axis.tick_params(axis='y', colors='white')
    axis.grid(color='white', linestyle='--', alpha=0.5)
    axis.plot(x, y)

    axis = fig.add_subplot(4, 4, 11)
    x = [1, 2, 3, 4]            #currently dummy data
    y = [7, 7, 4, 3]
    axis.title.set_text("Write Latency")
    axis.set_xlabel("Time")
    axis.set_ylabel("Latency (milliseconds)")
    axis.set_facecolor('#1F2326')
    axis.xaxis.label.set_color('white')
    axis.yaxis.label.set_color('white')
    axis.title.set_color('white')
    axis.spines['bottom'].set_color('white')
    axis.spines['left'].set_color('white')
    axis.tick_params(axis='x', colors='white')
    axis.tick_params(axis='y', colors='white')
    axis.grid(color='white', linestyle='--', alpha=0.5)
    axis.plot(x, y)

    axis = fig.add_subplot(4, 4, 12)
    x = [1, 2, 3, 4]            #currently dummy data
    y = [3, 5, 4, 1]
    axis.title.set_text("Read Latency")
    axis.set_xlabel("Time")
    axis.set_ylabel("Latency (milliseconds)")
    axis.set_facecolor('#1F2326')
    axis.xaxis.label.set_color('white')
    axis.yaxis.label.set_color('white')
    axis.title.set_color('white')
    axis.spines['bottom'].set_color('white')
    axis.spines['left'].set_color('white')
    axis.tick_params(axis='x', colors='white')
    axis.tick_params(axis='y', colors='white')
    axis.grid(color='white', linestyle='--', alpha=0.5)
    axis.plot(x, y)

    axis = fig.tight_layout()               #set the style of layout for the figure

    return fig


if __name__ == '__main__':
    webbrowser.open_new('http://localhost:4449/')
    app.run('localhost', 4449)




